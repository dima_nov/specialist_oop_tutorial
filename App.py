from Lamp import Lamp
from Point import Point, Point3D, Color, Human

lamp1 = Lamp(1)
print(lamp1.lamp_state)
lamp1.__state = True
lamp1.floor = 2
print(lamp1.lamp_state)
print(lamp1.floor)

lamp2 = Lamp(2)
lamp2.power = 80
print(lamp2.power)
print(lamp2.brand)
print(Lamp.count)
print(lamp1.show_brand())


my_point = Point(10, 20)
point2 = Point(22,33)
print(my_point)
my_point.move_to(20, 30)
print(my_point)

print(my_point.coordinates)
my_point.coordinates = [200,300]
print(my_point)
print(point2.count_points())
Point.count = 100
print(point2.count_points())

point_3d = Point3D(55,66,77)
point_3d.move_by(1,1,1)
print(point_3d)
print(Color.BLUE.value)
print(type(point_3d))
point_3d.move_by(1000, 2000, 3000)
print(point_3d)

john = Human('John', 'm')
ann = Human('Ann', 'w')
john + ann
baby = john * ann
print(john.status.name)
print(ann.status.name)
print(baby.sex)