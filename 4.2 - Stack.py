# Abstract data structure: Stack

# push(item)
# pop()
# peek()
# is_empty()
# size()

class Stack:
    def __init__(self):
        self.__data = list()

    def push(self, item):
        self.__data.append(item)

    def pop(self):
        if len(self.__data) > 0:
            return self.__data.pop()
        return None

    def peek(self):
        if len(self.__data) > 0:
            return self.__data[len(self.__data)-1]
        return None

    def is_empty(self):
        return len(self.__data) == 0

    def size(self):
        return len(self.__data)

    def show(self):
        print('\n'.join(str(val) for val in self.__data))

my_stack = Stack()
print(my_stack.is_empty())
print(my_stack.size())
my_stack.push([1,2,3,4,5])
my_stack.push('Dima')
my_stack.push(123123123)
my_stack.push([1,2,3,4,5])

def long_list(n):
    return list(range(1, n+1))
list = long_list(100)
my_stack.push(list)

print(my_stack.peek())
my_stack.show()
