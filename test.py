class Unit:
    def __init__(self, name, power):
        self.name = name
        self.power = power


import random


class Game:

    @staticmethod
    def fight():
        unit1 = Unit('Unit_1', random.randint(1, 5))
        unit2 = Unit('Unit_2', random.randint(1, 5))
        if unit1.power > unit2.power:
            res = unit1.power - unit2.power
            print(unit1.name, 'win', res)
        else:
            res = unit2.power - unit1.power
            print(unit2.name, 'win', res)

    @classmethod
    def run(cls):
        while True:
            question = input('Играем? y/n \n')
            if question == 'n':
                break
            cls.fight()
            if input('Продолжим? y/n \n') == 'n':
                break
        print("Спасибо за игру!")


Game.run()
