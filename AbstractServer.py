import abc

class AbstractServer(abc.ABC):
    @abc.abstractmethod
    def connect(self):
        pass

    @abc.abstractmethod
    def disconnect(self):
        pass

    @abc.abstractmethod
    def request(self):
        pass

    @abc.abstractmethod
    def response(self):
        pass


class ServerAppache(AbstractServer):
    def __init__(self, ver):
        if ver == '100.1.0':
            print('Morning!!!')

        self.ver = ver
        self.check_availability(self)

    def check_availability(self, server):
        if self.ver == '100.1.0':
            server.connect()
        else:
            raise Exception('Cant be com=nnected')

    def connect(self):
        print('Server connected')

    def disconnect(self):
        print('Server disconnected')

    def request(self):
        print('Request sent')

    def response(self):
        print('Responce accepted')

appache = ServerAppache('100.1.0')
# appache.check_availability(appache, '100.1.0.')
appache.request()