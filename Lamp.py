class Lamp():
    brand = "Philips"
    count = 0
    def __init__(self, floor=0):
        self.__state = False
        self.__floor = floor
        self.__power = 100
        self.__class__.count += 1

    def get_state(self):
        return self.__state
    lamp_state = property(get_state)

    def get_floor(self):
        return self.__floor

    def set_floor(self, v):
        self.__floor = v
    floor = property(get_floor, set_floor)

    @property
    def power(self):
        return self.__power

    @power.setter
    def power(self, v):
        self.__power = v


    def switch_on(self):
        if not self.state:
            self.state = True
            print('Switched on')

    def switch_off(self):
        if self.state:
            print('Switched off')
            self.state = False

    def __repr__(self):
        return f'I\'m a lamp on the {self.floor} '

    @classmethod
    def show_brand(cls):
        return cls.brand
