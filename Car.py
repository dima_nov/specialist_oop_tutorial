"""Создать класс Car, у которого есть характеристики: марка, год выпуска, цвет, объем двигателя.
Создать класс CarPark, у которого есть характеристики: адрес, название, список машин
Создать класс Info, который наполняет автопарк данными и машинами и выводит информацию об автопарке и всех машинах этого автопарка.
Используйте getters & setters.
Учтите:
- год выпуска не может быть меньше 2000 и больше 2020
- объем двигателя не может быть меньше 1000 и больше 6000"""

class Car:
    all_cars = []

    def __init__(self, make, year, color, v):
        self.make = make
        self.year = year
        self.color = color
        self.v = v
        self.all_cars.append(self.make)


class CarPark:
    def __init__(self, address, name, list_cars):
        self.address = address
        self.name = name
        self.list_cars = list_cars

class Info:
    audi = Car('Audi', 2010, 'Red', 1500)
    bmw = Car('BMW', 2011, 'Blue', 1600)
    mers = Car('Mersedes', 2012, 'Green', 1700)
    park = CarPark('Minsk', 'Super Park', [audi.__dict__, bmw.__dict__, mers.__dict__])



print(Info.park.__dict__)