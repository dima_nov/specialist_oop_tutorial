import timeit
import time

Timer = timeit.Timer


def fib(n):
    if n <= 2:
        return 1
    else:
        return fib(n - 2) + fib(n - 1)


def fib2(n):
    if n <= 2:
        return 1
    else:
        f = [0, 1, 1]
        for i in range(3, n):
            f.append(f[i - 2] + f[i - 1])
            print(i, f, f[i])


print(fib2(10))


def fib3(n):
    a, b = 0, 1
    for _ in range(n - 1):
        a, b = b, a + b
    return b


print(fib3(10))


def rev(s):
    reversed = ''
    ind = len(s) - 1
    while ind >= 0:
        reversed += s[ind]
        ind -= 1
    return reversed


def rev1(s):
    res = reversed(s)
    return res


print(rev('qazwsxedcrfvtgbyhnujmik,ol.p;'))

# t1 = Timer("rev('qazwsxedcrfvtgbyhnujmik,ol.p;')", 'from __main__ import rev')
# print('one', t1.timeit(), 's')
# t2 = Timer("rev1('qazwsxedcrfvtgbyhnujmik,ol.p;')", 'from __main__ import rev1')
# print('one', t2.timeit(), 's')


a = [1, 2]
a[0], a[1] = a[1], a[0]

print(a)


def buble_sort(arr):
    for i in range(len(arr)-1):
        print(arr)
        for i in range(len(arr)-1):
            if arr[i] > arr[i+1]:
                arr[i], arr[i+1] = arr[i+1], arr[i]
    return arr

print(buble_sort([5,4,3,2,1]))



def long_list(n):
    now = time.time()
    res = list(range(1, n+1))
    later = time.time()
    print((now-later))
    return res

def pop1(list, n):
    now = time.time()
    list.pop(n)
    later = time.time()
    print((now - later))
    return list

mylist = long_list(100000000)
pop1(mylist,len(mylist)-1)
pop1(mylist, 1)