class Dog:
    """Создать class Dog. У собаки должна быть кличка String name и возраст int age.
Создайте геттеры и сеттеры для всех переменных класса Dog."""
    __name = 'Doggie'
    _age = 12

    @property
    def gname(self):
        return self.__name

    @gname.setter
    def gname(self, n):
        self.__name = n

print(Dog().gname)
print(Dog._age)
Dog.gname = 'Terery'
print(Dog.gname)

