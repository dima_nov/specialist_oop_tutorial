import abc
import random


class IShape(abc.ABC):
    '''Интерфейс для реализации геометрических фигур'''

    @abc.abstractmethod
    def get_perimeter(self):
        '''Возвращает периметр фигуры'''
        pass

    @abc.abstractmethod
    def get_area(self):
        '''Возвращает площадь фигуры'''
        pass

    @abc.abstractmethod
    def get_description(self):
        '''Возвращает произвольное описание фигуры'''
        pass


class Circle(IShape):
    class_name = 'Круг'
    PI = 3.14

    def __init__(self, radius):
        self.radius = radius

    def get_perimeter(self):
        return 2 * self.PI * self.radius

    def get_area(self):
        return self.radius ** 2 * self.PI

    def get_description(self):
        print(f'Я {self.class_name} радиусом {self.radius}')


class Rectangle(IShape):
    class_name = 'Прямоугольник'

    def __init__(self, a, b):
        self.__a = a
        self.b = b

    def get_hight(self):
        return self.__a
    hight = property(get_hight)

    def get_width(self):
        return self.b
    width = property(get_width)

    def get_perimeter(self):
        perimeter = 2 * (self.__a + self.b)
        return perimeter

    def get_area(self):
        area = self.__a * self.b
        return area

    def get_description(self):
        print(f'Я {self.class_name} размером {self.__a} на {self.b}')


class Square(Rectangle):
    class_name = 'Квадрат'

    def __init__(self, side):
        super().__init__(side, side)

    def get_description(self):
        print(f'Я {self.class_name} со стороной {self.width}')


class Game:
    QUESTION_COUNT = 2
    def __init__(self):
        raise Exception('Нельзя создать экземпляр данного класс!')

    @staticmethod
    def get_shape():
        circle = Circle(random.randint(1, 5))
        rectangle = Rectangle(random.randint(1, 2), random.randint(1, 2))
        square = Square(random.randint(100, 200))
        shape = random.choice([circle, rectangle, square])
        shape.get_description()
        return shape

    @staticmethod
    def resolve(figure):
        user_input = input('Введи мой периметр\n')
        if user_input == str(figure.get_perimeter()):
            print('Правильно')
        else:
            print(f"Ошибка, првильный ответ {figure.get_perimeter()}")
        user_input = input('Введи мою площадь\n')
        if user_input == str(figure.get_area()):
            print('Правильно')
        else:
            print(f"Ошибка, првильный ответ {figure.get_area()}")

    @classmethod
    def calculate(cls):
            cls.resolve(cls.get_shape())

    @classmethod
    def run(cls):
        while True:
            print('Привет! Мы геометрические фигуры и у нас есть 2 вопроса.')
            question = input('Играем? y/n \n')
            if question == 'n':
                break
            cls.calculate()
            if input('Продолжим? y/n \n') == 'n':
                break
        print("Спасибо за игру!")


Game.run()
