
def f2(func):
    def wrapper(*a, **k):
        print('Before func')
        val = func(*a, **k)
        print('After func')
        return val
    return wrapper

@f2
def f1(a, b=2):
    print(f'{a}  {b} Called f1')

@f2
def add(x,y):
    return x + y

f1('Hi')
print(add(3,4))

class Test:
    @f2
    def decorated_method(self):
        print('run')


t = Test()
t.decorated_method()

import time

def timer(func):
    def wrapper():
        before = time.time()
        func()
        print('Function took:', time.time() - before, 'seconds')
    return wrapper

@timer
def run():
    time.sleep(2)

run()