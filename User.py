class User:
    count = 0
    def __init__(self, n, l, p):
        self._name = n
        self._login = l
        self._password = p
        self.__class__.count += 1

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, v):
        self._name = v

    @property
    def login(self):
        return self._login

    @login.setter
    def login(self, v):
        return None
        # raise AttributeError('Value can\'t be changed')

    @property
    def password(self):
        return '*******'

    @password.setter
    def password(self, v):
        self._password = v

    def show_info(self):
        print(f'This is {self.name} login: {self.login}')

    def info(self):
        for k, v in vars(self).items():
            print(k, v)


class SuperUser(User):
    count = 0
    def __init__(self, name, login, password, role):
        super().__init__(name, login, password)
        self._role = role

    @property
    def role(self):
        return self._role

    @role.setter
    def role(self, v):
        self._role = v

admin = SuperUser('John Lennon', 'john', '0000', 'admin')

user1 = User('Paul McCartney', 'paul', '1234')
user2 = User('George Harrison', 'george', '5678')
user3 = User('Richard Starkey', 'ringo', '8523')
admin1 = SuperUser('John Lennon1', 'john', '0000', 'admin')

print(user1.name)
user2.show_info()
user3.login = 'Monroe'
admin.show_info()

users = User.count
admins = SuperUser.count
print(f'Всего обычных пользователей: {users}') # Всего обычных пользователей: 3
print(f'Всего супер-пользователей: {admins}') # Всего супер-пользователей: 1

user1.info()


class Info:
    def p_info(self, users):
        for user in users:
            print(f'{user.name} {user.password} {user.login}')


info = Info()
info.p_info([user1,user2,user3])

