from enum import Enum


class Point:
    __count = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.__class__.__count += 1

    def move_to(self, x, y):
        self.x = x
        self.y = y

    @property
    def coordinates(self):
        return f'{self.x}: {self.y}'

    @coordinates.setter
    def coordinates(self, coord):
        self.x = coord[0]
        self.y = coord[1]

    def __repr__(self):
        return f'I\'m an object of {self.__class__.__name__}: {self.x} x {self.y}'

    @classmethod
    def count_points(cls):
        return cls.__count


class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3


class Point3D(Point):
    def __init__(self, x, y, z):
        super().__init__(x, y)
        self.z = z

    def move_by(self, x, y, z):
        self.x += x
        self.y += y
        self.z += z

    def __repr__(self):
        return f'I\'m an object of {self.__class__.__name__}: {self.x} x ' \
               f'{self.y} x {self.z}'


import random


class Human:
    def __init__(self, name, sex):
        self.name = name
        self.sex = sex.upper()
        self.status = None

    def __add__(self, human):
        if isinstance(human, Human):
            self.status = human
            human.status = self
        else:
            raise TypeError('Object not a human!')

    def __mul__(self, partner):
        if self.status == partner and self.sex != partner.sex:
            return Human('NewName', random.choice(['M', 'W']))
        else:
            raise Exception('This persons can\'t do the children')

