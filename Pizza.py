import math

class Pizza:
    def __init__(self, radius, ingredients):
        self.ingredients = ingredients
        self.radius = radius

    def __repr__(self):
        return f'{self.__class__.__name__} {self.radius} ({self.ingredients})'

    def area(self):
        return self.circle_area(self.radius)

    @staticmethod
    def circle_area(r):
        return r ** 2 * math.pi

    @classmethod
    def margherita(cls):
        return cls(['mozzarella', 'tomatoes'])

    @classmethod
    def prosciutto(cls):
        return cls(['mozzarella', 'tomatoes', 'ham'])


pizza = Pizza(4, ['mozzarella', 'tomatoes', 'ham'])
# print(Pizza.margherita())
# print(Pizza.prosciutto())
print(Pizza.circle_area(5))
print(pizza.circle_area(6))
print(pizza.area())
